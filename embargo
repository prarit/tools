#!/bin/bash
# SPDX-License-Identifier: GPL-2.0
#
# embargo
#
# This script originally queried the RHEL embargo-free git trees for the
# top-of-tree kernel tags for use by our partner engineers.  The embargo-free
# kernel trees were originally set up as a safe zone for our patner engineers
# as it was possible that the main kernel trees had been either security or
# feature frozen, aka embargoed, such that the kernel could not be externally
# released for testing.
#
# Eventually the kernel maintainer development process changed, and
# security and features were blacklisted and only released to the git
# trees when they were publically released.  At that time the script changed
# to reference the main kernel development git trees.  As time went on,
# another issue was noticed in which kernel-QE had not signed-off on a
# particular version of the kernel but it was being marked as available
# in this script.  This difference led to the possibility of our partner
# engineers testing kernels that kernel-QE had rejected.
#
# This script then pulled the latest tested kernel versions from brew.  This
# information was useful to both partner engineers and Red Hat engineers and
# is used to determine which kernels it is safe to test on.
#
# However, as time went on it was determined that too was insufficient in
# the case where a CVE/embargoed patch was applied to the kernel and discovered
# later on.  The only solution to this problem was for the maintainers to
# update the patchwork pages with a build number that the rest of can use.
#
# Fast forward to today, the correct way to now determine if a kernel is
# embargoed is to check brew to see if the build has a "-nocompose" tag.
#
# For example,
#
#	brew buildinfo kernel-3.10.0-1160.26.1.el7 | grep Tags: | grep nocompose
#
# will return 0 (success) if the build is embargoed.
#

get_latest_nvr() {
	local n
	local name
	local nID
	local z
	local zID
	osver=$1
	name=$2

	n=$(brew latest-build --quiet rhel-"${osver}"-candidate "${name}" 2> /dev/null | cut -d" " -f1)
	[[ "$n" == "" ]] && n=$(brew latest-build --quiet rhel-"${osver}"-beta-candidate "${name}" 2> /dev/null | cut -d" " -f1)

	z=$(brew latest-build --quiet rhel-"${osver}"-z-candidate "${name}" 2> /dev/null | cut -d" " -f1)

	# kernels are always of the form, kernel-4.18.0-ID.ID1.ID2....el8
	# if z doesn't have the same ID as n, then z-stream is not valid for this os version

	nID=$(echo "$n" | cut -d"-" -f3 | cut -d"." -f1)
	[ -z "$nID" ] && echo "" && return
	zID=$(echo "$z" | cut -d"-" -f3 | cut -d"." -f1)
	[ -z "$zID" ] && echo "$n" && return
	[ "$zID" -eq "$nID" ] && echo "$z" || echo "$n"
}

is_build_embargoed() {
	local out
	# return is 0 for embargoed, 1 for not embargoed, and 2 for nvr not found
	out=$(brew buildinfo "$1" 2> /dev/null)
	[ -z "$out" ] && return 2
	return $(echo "$out" | grep "Tags:" | grep -q nocompose)
}

verify_osver() {
	osver=$1
	[[ $osver == 7* ]] && return
	[[ $osver == 8* ]] && [ ! "$(echo $osver | xargs | wc -c)" -le 4 ] && return
	[[ $osver == 9* ]] && [ ! "$(echo $osver | xargs | wc -c)" -le 4 ] && return
	osver=${osver}.0
}

check_embargo() {
	local failmsg
	local name
	local nvr
	name="$2"
	failmsg="$3"
	verify_osver "$1"
	nvr=$(get_latest_nvr "$osver" "$name")
	if [ -z "$nvr" ]; then
		[ "$failmsg" -eq 1 ] && echo "invalid OS version $1"
	else
		is_build_embargoed "$nvr" && echo "WARNING: $osver build $nvr is embargoed.  See 'embargo --help' for finding a non-embargoed build." || echo "$osver build $nvr is not embargoed."
	fi
}

check_nvr_embargo() {
	local max
	local nvr
	local size

	osver=el$(echo "$1" | awk -F "[.]el" ' { print $2 } ' | cut -d"." -f1)
	# get the number of tokens in the NVR
	max=$(echo "$1" | awk -F "." '{ print NF }')
	# Anything smaller than the first three tokens doesn't make sense.  ex) kernel-4.18.0
	# is not a valid build
	for size in $(seq $((max - 1)) -1 3)
	do
		# create an nvr string while dropping the last tokens
		nvr=$(echo "$1" | awk -F "." -v start=1 -v end="$((size+1))" '{for (i=start; i<=end;i++) printf("%s%s", $i,(i==end) ? "\n" : ".") }')
		[ "$size" -ne $((max -1)) ] && nvr="${nvr}.$osver"
		is_build_embargoed "$nvr"
		case $? in
		0)
			if [ "$nvr" == "$1" ]; then
				echo "WARNING: Build $nvr is embargoed.  See 'embargo --help' for finding a non-embargoed build."
			else
				echo "WARNING: Base build $nvr for $1 is embargoed.  See 'embargo --help' for finding a non-embargoed build."
			fi
			exit 0
			;;
		1)
			if [ "$nvr" == "$1" ]; then
				echo "Build $nvr is not embargoed."
			else
				echo "Base build $nvr for $1 is not embargoed."
			fi
			exit 0
			;;
		default)
			;;
		esac
	done
	echo "ERROR: invalid NVR $1"
	exit 1
}

usage() {
	echo "usage: embargo [<release_version>|<nvr>|all]  # If no release_version is specified all OS"
	echo "                                        # information is displayed"
	echo "-h|--help    help (this message)"
	echo " "
	echo "ex) embargo 7.2"
	echo "ex) embargo 8.2.0"
	echo "ex) embargo 7.9"
	echo "ex) embargo 8.4 kernel-rt"
	echo "ex) embargo 8.2 mcelog # for mcelog version"
	echo "ex) embargo kernel-3.10.0-123.20.1.el7"
	echo "ex) embargo kernel-3.10.0-123.20.1.mr123_20150528_1233.el7"
	echo "ex) embargo all # for all OSes"
	echo "ex) embargo all kernel# kernel package for all OSes"
	echo " "
	echo "Finding a non-embargoed build: If the embargo command has WARNED that a build is embargoed try"
	echo "searching previous build versions.  For example, if kernel-4.18.0-200.el8 is embargoed, try"
	echo "kernel-4.18.0-199.el8, etc."
}

# output help
[ $# -eq 0 ] && usage && exit 1
[ "$1" == "-h" ] || [ "$1" == "--help" ] && usage && exit 1

if [ $# -le 2 ] && [ "$1" != "all" ]; then
	[ $# -eq 1 ] && name=kernel || name="$2"
	if echo "$1" | grep -q "-"; then
		check_nvr_embargo "$1"
	else
		check_embargo "$1" "$name" 1
	fi
else
	[ $# -eq 1 ] && name=kernel || name="$2"
	for x in 7 8 9 10 11 12
	do
		for y in $(seq 0 9)
		do
			check_embargo "$x.$y" "$name" 0
		done
	done
fi

#!/bin/bash
# SPDX-License-Identifier: GPL-2.0

# Generate a list of commits from --upstream missing in --downstream based on
# various criteria, such as file keywords, file lists, author, committer,
# merger, optionally between specific --base and --target versions, and
# logging commits referenced as fixing or fixed-by those commits.

set -e

function usage () {
	echo "Usage: $(basename "$0") [options] --downstream <name>"
	echo "Options:"
	echo "--base <object>            Starting object to search, ex. commit ID or tag"
	echo "                           Default: base of downstream branch"
	echo "--target <object>          End object to search, ex. commit ID or tag"
	echo "                           Default: HEAD of upstream remote"
	echo "--upstream <name>          Name of upstream remote, default \"origin\""
	echo "--upstream-branch <name>   Name of upstream branch, default \"master\""
	echo "--downstream <name>        Name of downstream remote"
	echo "--downstream-branch <name> Name of downstream branch, default \"main\""
	echo "--file-keyword <word>      Search all files containing this keyword,"
	echo "                           ex. \"vfio\", \"pci\", \"mlx5\""
	echo "--file-list <file>         Search files listed in provided file"
	echo "--authored                 Include commits authored by you"
	echo "--author <author>          Include commits authored by <author>"
	echo "--committed                Include commits committed by you"
	echo "--committer <committer>    Include commits committed by <committer>"
	echo "--merged                   Include commits merged by you"
	echo "--merger <merger>          Include commits merged by <merger>"
	echo "--skip-fixes               Don't generate fixes/fixed-by logs"
	echo "--skip-update              Don't update base or target remotes"
	echo "--skip-tags                Don't lookup tags for commits"
	echo "--skip-conflicts           Don't look for conflicts against MRs"
	echo "--exclude-local            Exclude commits found in working branch"
	echo "--list-excluded            List commits excluded by existing reference"
	echo "--exclude-merges <commits> Comma separated list of merges to exclude"
	echo "--chronological            Chronological patch order"
	echo "--backport                 Interactive backport prompt for each commit"
	echo "                           Implies chronological, uses git-backport"
	echo "--output <file>            Write commits to provided file"
	echo "--debug                    Extra debugging"
	echo "--help|-h                  This help text"
}

BASE=${BASE:-""}
TARGET=${TARGET:-""}
UPSTREAM=${UPSTREAM:-"origin"}
UPSTREAM_BRANCH=${UPSTREAM_BRANCH:-"master"}
DOWNSTREAM=${DOWNSTREAM:-""}
DOWNSTREAM_BRANCH=${DOWNSTREAM_BRANCH:-"main"}
KEYWORD=${KEYWORD:-""}
FILE_LIST=${FILE_LIST:-""}
AUTHORED=${AUTHORED:-0}
AUTHOR=${AUTHOR:-""}
COMMITTED=${COMMITTED:-0}
COMMITTER=${COMMITTER:-""}
MERGED=${MERGED:-0}
MERGER=${MERGER:-""}
SKIP_FIXES=${SKIP_FIXES:-0}
SKIP_UPDATE=${SKIP_UPDATE:-0}
SKIP_TAGS=${SKIP_TAGS:-0}
SKIP_CONFLICTS=${SKIP_CONFLICTS:-0}
EXCLUDE_LOCAL=${EXCLUDE_LOCAL:-0}
LIST_EXCLUDED=${LIST_EXCLUDED:-0}
EXCLUDE_MERGES=${EXCLUDE_MERGES:-""}
CHRONOLOGICAL=${CHRONOLOGICAL:-0}
BACKPORT=${BACKPORT:-0}
OUTPUT=${OUTPUT:-""}
DEBUG=${DEBUG:-0}


while [[ $# -gt 0 ]]; do
	key="$1"
	shift
	case $key in
		--base)
			BASE=$1
			shift
			;;
		--target)
			TARGET=$1
			shift
			;;
		--upstream)
			UPSTREAM=$1
			shift
			;;
		--upstream-branch)
			UPSTREAM_BRANCH=$1
			shift
			;;
		--downstream)
			DOWNSTREAM=$1
			shift
			;;
		--downstream-branch)
			DOWNSTREAM_BRANCH=$1
			shift
			;;
		--file-keyword)
			KEYWORD=$1
			shift
			;;
		--file-list)
			FILE_LIST=$1
			shift
			;;
		--authored)
			AUTHORED=1
			;;
		--author)
			AUTHOR=$1
			shift
			;;
		--committed)
			COMMITTED=1
			;;
		--committer)
			COMMITTER=$1
			shift
			;;
		--merged)
			MERGED=1
			;;
		--merger)
			MERGER=$1
			shift
			;;
		--skip-fixes)
			SKIP_FIXES=1
			;;
		--skip-update)
			SKIP_UPDATE=1
			;;
		--skip-tags)
			SKIP_TAGS=1
			;;
		--skip-conflicts)
			SKIP_CONFLICTS=1
			;;
		--exclude-local)
			EXCLUDE_LOCAL=1
			;;
		--list-excluded)
			LIST_EXCLUDED=1
			;;
		--exclude-merges)
			EXCLUDE_MERGES=$1
			shift
			;;
		--chronological)
			CHRONOLOGICAL=1
			;;
		--backport)
			BACKPORT=1
			CHRONOLOGICAL=1
			;;
		--output)
			OUTPUT=$1
			echo -n > "$OUTPUT"
			shift
			;;
		--debug)
			DEBUG=1
			LIST_EXCLUDED=1
			;;
		--help|-h)
			usage
			exit 0
			;;
		*)
			echo "Unknown option $key"
			usage
			exit 1
			;;
	esac
done

LAST=0

function backport () {
	while true; do
		read -rp "(B)ackport, [N]ext, (Q)uit, (S)how: " VAL
		VAL=${VAL:-"N"}
		case $VAL in
			N|n|Next|NEXT|next)
				break
				;;
			S|s|Show|SHOW|show)
				git show "$1"
				;;
			B|b|Backport|BACKPORT|backport)
				git-backport --last $LAST "$1"
				LAST=$(( LAST + 1 ))
				break
				;;
			Q|q|Quit|QUIT|quit)
				exit 1
				;;
			*)
				;;
		esac
	done
}

if [ -z "$DOWNSTREAM" ]; then
	usage
	exit 1
fi

if [ $SKIP_UPDATE -eq 0 ]; then
	echo Updating "$DOWNSTREAM" >&2
	git remote update "$DOWNSTREAM" > /dev/null 2>&1
	echo Updating "$UPSTREAM" >&2
	git remote update "$UPSTREAM" > /dev/null 2>&1
fi

echo "$DOWNSTREAM" HEAD: "$(git describe "$DOWNSTREAM"/"$DOWNSTREAM_BRANCH")"
echo "$UPSTREAM" HEAD: "$(git describe "$UPSTREAM"/"$UPSTREAM_BRANCH")"

if [ -z "$BASE" ]; then
	# Try to strip the kernel version from the description, looking for anything
	# with x.y.z format, stripping the trailing .0 if present to match tag names
	BASE=v"$(git describe "$DOWNSTREAM"/"$DOWNSTREAM_BRANCH" | grep -oe '[0-9]\.[0-9]\+\.[0-9]\+' | sed -e 's/\.0$//')"
else
	set +e
	if ! git describe "$BASE" > /dev/null 2>&1; then
		echo Invalid base object "$BASE"
		exit 1
	fi
	set -e
fi

echo Using base of "$(git describe "$BASE")"

if [ -z "$TARGET" ]; then
	TARGET="$UPSTREAM"/"$UPSTREAM_BRANCH"
else
	set +e
	if ! git describe "$TARGET" > /dev/null 2>&1; then
		echo Invalid target object "$TARGET"
		exit 1
	fi
	set -e
fi

echo Using target of "$(git describe "$TARGET")"

FILES_LIST=$(mktemp)
COMMITS_LIST=$(mktemp)
FULL_COMMITS_LIST=$(mktemp)
DOWNSTREAM_LOG=$(mktemp)
TMP=$(mktemp)
TMP2=$(mktemp)

function cleanup {
	rm -f "$FILES_LIST" "$COMMITS_LIST" "$FULL_COMMITS_LIST" "$DOWNSTREAM_LOG" "$TMP" "$TMP2"
}

trap cleanup EXIT

if [ -n "$KEYWORD" ]; then
	git ls-tree -r --name-only "$BASE" | grep "$KEYWORD" >> "$FILES_LIST"
	git ls-tree -r --name-only "$TARGET" | grep "$KEYWORD" >> "$FILES_LIST"
fi

if [ -n "$FILE_LIST" ]; then
	cat "$FILE_LIST" >> "$FILES_LIST"
fi
readarray -t FILES < <(sort -u "$FILES_LIST")
COMMITS=0
if [ ${#FILES[@]} -ne 0 ]; then
	echo Looking for file commits... >&2
	git log --no-merges --pretty=format:%H "$BASE".."$TARGET" -- "${FILES[@]}" >> "$COMMITS_LIST"
	sed -i -e '$a'\\ "$COMMITS_LIST"
	if [ $DEBUG -ne 0 ]; then
		echo Added $(( $(wc -l "$COMMITS_LIST" | awk '{print $1}') - COMMITS )) commits
		COMMITS=$(wc -l "$COMMITS_LIST" | awk '{print $1}')
	fi
fi

if [ $AUTHORED -ne 0 ]; then
	echo Looking for commits authored by "$(git config user.email)"... >&2
	git log --no-merges --pretty=format:%H --author="$(git config user.email)" "$BASE".."$TARGET" >> "$COMMITS_LIST"
	sed -i -e '$a'\\ "$COMMITS_LIST"
	if [ $DEBUG -ne 0 ]; then
		echo Added $(( $(wc -l "$COMMITS_LIST" | awk '{print $1}') - COMMITS )) commits
		COMMITS=$(wc -l "$COMMITS_LIST" | awk '{print $1}')
	fi
fi

if [ -n "$AUTHOR" ]; then
	echo Looking for commits authored by "$AUTHOR"... >&2
	git log --no-merges --pretty=format:%H --author="$AUTHOR" "$BASE".."$TARGET" >> "$COMMITS_LIST"
	sed -i -e '$a'\\ "$COMMITS_LIST"
	if [ $DEBUG -ne 0 ]; then
		echo Added $(( $(wc -l "$COMMITS_LIST" | awk '{print $1}') - COMMITS )) commits
		COMMITS=$(wc -l "$COMMITS_LIST" | awk '{print $1}')
	fi
fi

if [ $COMMITTED -ne 0 ]; then
	echo Looking for commits by "$(git config user.email)"... >&2
	git log --no-merges --pretty=format:%H --committer="$(git config user.email)" "$BASE".."$TARGET" >> "$COMMITS_LIST"
	sed -i -e '$a'\\ "$COMMITS_LIST"
	if [ $DEBUG -ne 0 ]; then
		echo Added $(( $(wc -l "$COMMITS_LIST" | awk '{print $1}') - COMMITS )) commits
		COMMITS=$(wc -l "$COMMITS_LIST" | awk '{print $1}')
	fi
fi

if [ -n "$COMMITTER" ]; then
	echo Looking for commits by "$COMMITTER"... >&2
	git log --no-merges --pretty=format:%H --committer="$COMMITTER" "$BASE".."$TARGET" >> "$COMMITS_LIST"
	sed -i -e '$a'\\ "$COMMITS_LIST"
	if [ $DEBUG -ne 0 ]; then
		echo Added $(( $(wc -l "$COMMITS_LIST" | awk '{print $1}') - COMMITS )) commits
		COMMITS=$(wc -l "$COMMITS_LIST" | awk '{print $1}')
	fi
fi

set +e
if [ $MERGED -ne 0 ]; then
	echo Looking for merges by "$(git config user.email)"... >&2
	for ID in $(git log --merges --author="$(git config user.email)" --pretty=format:%H "$BASE".."$TARGET"); do
		if echo "$EXCLUDE_MERGES" | grep -q "$ID"; then
			continue
		fi
		git log --no-merges --pretty=format:%H "$ID"^.."$ID" >> "$COMMITS_LIST"
		sed -i -e '$a'\\ "$COMMITS_LIST"
		if [ $DEBUG -ne 0 ]; then
			echo Added $(( $(wc -l "$COMMITS_LIST" | awk '{print $1}') - COMMITS )) commits from merge "$ID"
			COMMITS=$(wc -l "$COMMITS_LIST" | awk '{print $1}')
		fi
	done
fi

if [ -n "$MERGER" ]; then
	echo Looking for merges by "$MERGER"... >&2
	for ID in $(git log --merges --author="$MERGER" --pretty=format:%H "$BASE".."$TARGET"); do
		if echo "$EXCLUDE_MERGES" | grep -q "$ID"; then
			continue
		fi
		git log --no-merges --pretty=format:%H "$ID"^.."$ID" >> "$COMMITS_LIST"
		sed -i -e '$a'\\ "$COMMITS_LIST"
		if [ $DEBUG -ne 0 ]; then
			echo Added $(( $(wc -l "$COMMITS_LIST" | awk '{print $1}') - COMMITS )) commits from merge "$ID"
			COMMITS=$(wc -l "$COMMITS_LIST" | awk '{print $1}')
		fi
	done
fi
set -e

sort -o "$TMP" "$COMMITS_LIST"
uniq "$TMP" "$COMMITS_LIST"

COMMITS=$(wc -l "$COMMITS_LIST" | awk '{print $1}')
if [ "$COMMITS" -eq 0 ]; then
	echo No commits found, try providing file keyword, file list, author, committer, or merger
	usage
	exit 1
fi

echo Analyzing "$COMMITS" commits >&2

echo Generating "$DOWNSTREAM" log... >&2
git log "$BASE".."$DOWNSTREAM"/"$DOWNSTREAM_BRANCH" > "$DOWNSTREAM_LOG"

# $FULL_COMMITS_LIST excludes commits in $DOWNSTREAM but not in local branch
# so that we can still report merge conflicts against local commits
grep -of "$COMMITS_LIST" "$DOWNSTREAM_LOG" | sort -u > "$TMP"
grep -vf "$TMP" "$COMMITS_LIST" > "$FULL_COMMITS_LIST"

NAME="$DOWNSTREAM"

if [ $EXCLUDE_LOCAL -ne 0 ]; then
	git log "$DOWNSTREAM"/"$DOWNSTREAM_BRANCH"..HEAD >> "$DOWNSTREAM_LOG"
	NAME+=" & local"
fi

# Expect full SHA1 in downstream log if included
grep -of "$COMMITS_LIST" "$DOWNSTREAM_LOG" | sort -u > "$TMP"
echo "Excluding $(wc -l "$TMP" | awk '{print $1}') commits referenced in ""$NAME"" logs" >&2
if [ $LIST_EXCLUDED -ne 0 ]; then
	while IFS= read -r ID; do
		git log --no-decorate --oneline -1 "$ID"
	done < "$TMP"
fi
grep -vf "$TMP" "$COMMITS_LIST" > "$TMP2"

echo Generating order reference... >&2
if [ $CHRONOLOGICAL -eq 0 ]; then
	git log --topo-order --no-merges --pretty=format:%H "$BASE".."$TARGET" > "$TMP"
else
	git log --topo-order --no-merges --pretty=format:%H "$BASE".."$TARGET" | tac > "$TMP"
fi
grep -f "$TMP2" "$TMP" > "$COMMITS_LIST"

echo Upstream commits not found in "$DOWNSTREAM": >&2

# Various failures possible in loop
set +e

DESC_LAST=""
while IFS= read -r -u 999 ID; do # < "$COMMITS_LIST"
	if [ $SKIP_TAGS -eq 0 ]; then
		DESC=$(git describe --contains --tags --match "v[0-9]\.[0-9]*" "$ID" 2>/dev/null | sed 's/~.*//' | sed 's/\^.*//' | tr -d '\n')
		if [ -z "$DESC" ]; then
			DESC="untagged"
		fi
		if [ "$DESC" != "$DESC_LAST" ]; then
			echo "$DESC:"
			DESC_LAST="$DESC"
		fi
	fi

	echo "  $(git log --no-decorate --oneline -1 --pretty=format:"%h (\"%s\")" "$ID")"

	if [ -n "$OUTPUT" ]; then
		git log --no-decorate --oneline -1 --pretty=format:"%h (\"%s\")" "$ID" >> "$OUTPUT"
	fi

	if [ $SKIP_FIXES -ne 0 ]; then
		continue
	fi

	# What commits does this fix
	git log -1 --pretty=raw "$ID" | grep -E "[fF]ix.*: [a-f0-9]{8,12} \(\".+\"\)" | grep -oE "[a-f0-9]{8,12}" > "$TMP"
	if [ -s "$TMP" ]; then
		while IFS= read -r FIX; do
			FULL_HASH=$(git log -1 --pretty=format:%H "$FIX")
			echo -n "    Fixes: $(git log -1 --pretty="%h (\"%s\")" "$FIX")"
			if grep -q "$FULL_HASH" "$DOWNSTREAM_LOG"; then
				echo -e " [Referenced in $DOWNSTREAM]"
			else
				if git tag --contains "$FULL_HASH" | grep -q ^"$BASE"$; then
					echo -e " [Predates $BASE]"
				else
					echo
				fi
			fi
		done < "$TMP"
	else
		# If not a fix, was it nominated for stable
		git log -1 --pretty=raw "$ID" | grep "stable@"
	fi

	# What commits fix this commit
	SHORT=$(git log -1 --pretty=format:%h "$ID")
	git log --grep "[fF]ix.*: $SHORT" --pretty=format:%H "$ID".."$UPSTREAM"/"$UPSTREAM_BRANCH" > "$TMP"
	if [ -s "$TMP" ]; then
		while IFS= read -r FIX; do
			echo "    Fixed-by: $(git log --oneline -1 --pretty=format:"%h (\"%s\")" "$FIX")"
		done < "$TMP"
	fi

	if [ $BACKPORT -ne 0 ]; then
		backport "$ID"
	fi
done 999< "$COMMITS_LIST"

if [ $SKIP_CONFLICTS -eq 0 ]; then
	CONFLICTS=0
	for MERGE in $(git branch -r --no-merged "$DOWNSTREAM"/"$DOWNSTREAM_BRANCH" --list "$DOWNSTREAM"/merge-requests/*); do
		git log "$DOWNSTREAM"/"$DOWNSTREAM_BRANCH".."$MERGE" > "$TMP"
		if grep -qf "$FULL_COMMITS_LIST" "$TMP"; then
			if [ $CONFLICTS -eq 0 ]; then
				echo -e "\nFound merge conflicts:"
				CONFLICTS=1
			fi
			echo "  Conflicts found against merge request $MERGE"
			grep -of "$FULL_COMMITS_LIST" "$TMP" | sort -u | while IFS= read -r ID; do
				echo "    $(git log -1 --pretty="%h (\"%s\")" "$ID")"
			done
		fi
	done
fi

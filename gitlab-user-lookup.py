#!/usr/bin/env python
# SPDX-License-Identifier: GPL-2.0
import argparse
# pip install python3-gitlab
import gitlab
# pip install columnar
from columnar import columnar

parser = argparse.ArgumentParser()
parser.add_argument("input")
args = parser.parse_args()

try:
    gl = gitlab.Gitlab.from_config()
except Exception:
    parser.error("Please set up ~/.python-gitlab.cfg!\n" \
                 " => See https://python-gitlab.readthedocs.io/en/stable/cli.html")

print(f'Searching for users matching "{args.input}"...')
userlist = gl.users.list(search=args.input, as_list=False)

if len(userlist) == 0:
    print(f'No users found.')
    exit(1)

users = []
for user in userlist:
    details = gl.users.get(user.id)
    public_email = details.public_email
    users.append([user.username, str(user.id), details.public_email])

headers = ['USERNAME', 'ID', 'PUBLIC EMAIL']

table = columnar(users, headers, no_borders=True)
print(table)
